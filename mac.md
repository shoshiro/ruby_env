# インストールするもの
- Slack
- Visual Studio Code
- Homebrew
- rbenv
- Ruby
    - Rails

# インストール手順
以下の手順はmacOS向けとなります

## Visual Studio Codeのインストール
[Microsoftの公式ページ](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)からダウンロードして、インストールしましょう。

## Homebrewのインストール
macOS用パッケージを管理するソフトウェアです。  
参考リンク(https://brew.sh/index_ja)
- Homebrewインストール
```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

## rbenvのインストール
Rubyのバージョンを管理するためのパッケージです。
公式リポジトリ(https://github.com/rbenv/rbenv)

```
$ brew install rbenv
```
このままだとシステムのRubyを参照してしまうので、シェルの起動時に「rbenv init」をして自動的に設定を行うようにしましょう。
```
$ echo 'eval "$(rbenv init -)"' >> ~/.zshrc
```
ターミナルを再起動、もしくはsourceコマンドでシェルを再読込しましょう。
```
$ source ~/.zshrc
```

## Rubyのインストール
rbenvを使用してRubyをインストールしましょう(5分〜10分くらい時間がかかります)。
```
$ rbenv install 2.6.6
$ rbenv global 2.6.6
```
rubyのバージョンがデフォルトのものから切り替わっていれば成功です。
```
$ ruby -v
ruby 2.6.6p146 (2020-03-31 revision 67876) [x86_64-darwin19]
```
上記のような表示が出れば成功です。

## Railsのインストール
```
$ gem install rails
```

### うまく行かない場合
システムにデフォルトでインストールされているRubyを参照している可能性が高いです。
「rbenv init」を行いましょう。