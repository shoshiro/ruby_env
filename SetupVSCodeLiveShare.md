# Visual Studio Code Live Shareのインストール
Visual Studio Codeはソフトウェア開発に便利なテキストエディタですが、拡張機能によって様々な機能を追加できます。その一つに、`Live Share`というものがあり、これを使うと離れた場所にいる共同開発者の人と画面やファイルを共有しながら開発作業を行えます。

1. Visual Studio Codeを起動します
2. 画面左側のメニューアイコンのうち`Extensions`をクリックするか、`Ctrl + Shift + X`キーを押して、拡張機能の検索画面を表示します。
3. `Search Extensions in Market`と書かれたテキストボックスに`Live Share`と入力して検索します
4. 検索結果の上の方に表示される`Live Share`をクリックし、`Microsoft Visual Studio Live Share`と説明書きがされていることを確認し、`Install`ボタンをクリックします。
5. しばらく待って、インストールが完了すると、ウィンドウ左側のメニューアイコンが一つ増えて、`Live Share`という項目が追加されます。

# Live Shareでセッションを確立する
1. ウィンドウ左側のメニューアイコン`Live Share`をクリックします
2. `Share (Read/Write)`ボタンをクリックします
3. `Windows Defender ファイアウォールでブロックされています`などと書かれた注意を呼びかけるダイアログが表示されたら、`アクセスを許可する`をクリックします（これを許可しないと通信できません）。
4. ウィンドウ左側上部の`SESSION DETAILS`のうち、`Participants`欄にある`Invite participants`をクリックすると、共有リンクがコピーされるので、共同で作業するメンバーにSlackなどのテキストメッセージで伝えてください。

# ターミナルを共有する
Live Shareではターミナルを2人で同時に操作することができます。

1. Ctrl + `(キーボードの＠キーをShiftキーと同時に押すと入力できます)を押すと、ターミナルが表示されます
2. ウィンドウ左側の`SESSION DETAILS -> Shared Terminals`に`Powershell (Read-only)`などと表示されるはずです。このままでは、共有相手はコンソールを操作することができません。右クリックして`Make Read/Write`をクリックすると共有相手も操作できるようになります

# 相手の操作に追従する
Live Shareで共有中の相手の操作に追従する設定にすることで、相手が開いているファイルを切り替えたり、文字を入力したときに、状況をすぐ確認できるようになります。

1. ウィンドウ左側の`SESSION DETAILS -> Participants`にあるユーザ名をクリックします。
2. エディタの画面が相手の操作に追従するようになります

# ローカルサーバを共有する
Live Shareでは、共有元のPCで実行されているローカルサーバ(Railsなど)に共有中の相手のPCからアクセスすることができます。

1. `rails server`などのコマンドを実行し、ローカルサーバを立ち上げ、ポート番号を控えておきます
2. ウィンドウ左側の`SESSION DETAILS -> Shared Servers`にある`Share server...`をクリックし、先程控えたポート番号を入力して、`Enterキー`を押します
3. 共有中の相手のPCブラウザで`localhost:[ポート番号]`でアクセスしてもらえば閲覧できます

# Live Share Audioについて(オプション)
VScodeの拡張機能に`Live Share Audio`というものがあります。この拡張機能を使用すると、VScode上で共有中のユーザとボイスチャットを行うことができます。
